package com.demo.service;

import com.demo.pojo.User;

import java.util.Map;

public interface UserService {
    User getUser(String userId);
    Map<String, User> getUser();
}
