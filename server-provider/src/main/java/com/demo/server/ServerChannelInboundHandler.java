package com.demo.server;

import com.alibaba.fastjson.JSON;
import com.demo.model.RequestModel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import com.demo.model.ResponseModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.net.InetSocketAddress;

@ChannelHandler.Sharable
@Component
public class ServerChannelInboundHandler extends SimpleChannelInboundHandler<String> implements ApplicationContextAware {

    /**
     * 日志
     */
    private Logger log = LoggerFactory.getLogger(SimpleChannelInboundHandler.class);
    private ApplicationContext applicationContext;

    String requestId;

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String msg) {
        StringBuilder sb = null;
        RequestModel result = null;
        try {
            // 报文解析处理
            sb = new StringBuilder();
            result = JSON.parseObject(msg, RequestModel.class);

            requestId = result.getRequestId();

            String serviceName = result.getServiceName();

            String methodName = result.getMethodName();


            Class[] paramType = result.getParamTypes();

            Object[] paramValue = result.getParamValues();

            System.out.println(serviceName + "  " + methodName);


            String substring = serviceName.substring(serviceName.lastIndexOf(".") + 1);
            String s = substring.substring(0, 1).toLowerCase() + substring.substring(1);
            Object serviceObject = applicationContext.getBean(s);

            Method method = Class.forName(serviceName).getMethod(methodName, paramType);
            Object returnValue = method.invoke(serviceObject, paramValue);
            ResponseModel responseModel = new ResponseModel(requestId,serviceName,methodName,"200",JSON.toJSONString(returnValue));
            sb.append(JSON.toJSONString(responseModel));
            sb.append("\n");
            System.out.println(sb.toString());
            ctx.writeAndFlush(sb);
        } catch (Exception e) {
            ResponseModel responseModel = new ResponseModel(requestId,"","","500",e.getMessage());
            String errorCode = JSON.toJSONString(responseModel)+"\n";
            log.error(errorCode);
            ctx.writeAndFlush(errorCode);
            log.error("报文解析失败: " + e.getMessage());
        }
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        InetSocketAddress insocket = (InetSocketAddress) ctx.channel().remoteAddress();
        String clientIp = insocket.getAddress().getHostAddress();
        log.info("收到客户端[ip:" + clientIp + "]连接");
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        // 当出现异常就关闭连接
        ctx.close();
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
};

