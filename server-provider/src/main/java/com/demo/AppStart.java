package com.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class AppStart {
    public static void main(String[] args) {

        ConfigurableApplicationContext context = SpringApplication.run(AppStart.class, args);

        new Thread((Runnable) context.getBean("nettyServer")).start();



    }
}
