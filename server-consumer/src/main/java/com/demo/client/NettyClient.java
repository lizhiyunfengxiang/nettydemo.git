package com.demo.client;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * nettyClient
 */
public class NettyClient {

    private static volatile Channel channel = null;

    private NettyClient(){}

    public static Channel getChannel(String host ,int port){
        if (channel==null){
            synchronized (NettyClient.class){
                if(channel == null){
                    channel =createChannel(host,port);
                }
            }
        }
        return channel;
    }

    /**
     * 创建 channel.
     * @return
     */
    public static Channel createChannel(String host, int port) {
        EventLoopGroup group = new NioEventLoopGroup();
        try {
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(group).channel(NioSocketChannel.class);
            /**
             * 有数据就发送
             */
            bootstrap.option(ChannelOption.TCP_NODELAY, true);
            /**
             * 是否启用心跳保活机制。在双方TCP套接字建立连接后（即都进入ESTABLISHED状态）并且在两个小时左右上层没有任何数据传输的情况下，这套机制才会被激活。
             */
//            bootstrap.option(ChannelOption.SO_KEEPALIVE, true);
            bootstrap.handler(new ClientInitializer());
            Channel channel = bootstrap.connect(host, port).sync().channel();

            channel.closeFuture().addListener(future->{
                group.shutdownGracefully();
            });

            return channel;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}