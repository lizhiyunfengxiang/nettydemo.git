package com.demo.client;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.demo.AppStart;
import com.demo.model.ResponseModel;
import com.demo.pojo.User;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.concurrent.Promise;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * 客户端处理器 . <br>
 *
 */
@Slf4j
@ChannelHandler.Sharable
public class ClientHandler extends SimpleChannelInboundHandler<String>  {




    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String msg) {
        System.out.println("收到服务端消息: " + msg);

        ResponseModel responseModel = JSON.parseObject(msg,ResponseModel.class);

        String responseId = responseModel.getResponseId();


        Promise promise = LocalPromise.promiseMap.remove(responseId);

        if(promise != null){
            String code = responseModel.getCode();
            if(code.equals("200")){
                promise.setSuccess(responseModel.getData());
            }else{
                promise.setFailure(new RuntimeException(responseModel.getData()));
            }
        }


    }

    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
       log.error(cause.getMessage());
    }
}