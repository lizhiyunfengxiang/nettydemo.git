package com.demo;

import com.alibaba.fastjson.JSON;
import com.demo.client.LocalPromise;
import com.demo.client.NettyClient;
import com.demo.model.RequestModel;
import com.demo.pojo.User;
import com.demo.service.UserService;
import io.netty.channel.Channel;
import io.netty.util.concurrent.DefaultPromise;
import io.netty.util.concurrent.Promise;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Map;

@RestController
@SpringBootApplication
public class AppStart {


    @Value("${host}")
    private String host;
    @Value("${port}")
    private int port;

    public static void main(String[] args) {
        SpringApplication.run(AppStart.class, args);
    }

    private <T> T getProxyService(Class<T> serviceClass) {
        Object service = Proxy.newProxyInstance(serviceClass.getClassLoader(), new Class[]{serviceClass}, new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                Channel channel = NettyClient.getChannel(host, port);
                RequestModel requestModel = new RequestModel("100001", method.getDeclaringClass().getName(), method.getName(), method.getParameterTypes(), args);
                channel.writeAndFlush(JSON.toJSONString(requestModel) + "\n");
                Promise promise = new DefaultPromise(channel.eventLoop());
                LocalPromise.promiseMap.put(requestModel.getRequestId(), promise);

                System.out.println(LocalPromise.promiseMap+">>>>>>>>>>>>");
                promise.await();
                if (promise.isSuccess()) {
                    Class<?> returnType = method.getReturnType();
                    return JSON.toJavaObject(JSON.parseObject(promise.getNow()+""),returnType);
                } else {
                    System.out.println(promise.cause());
                    return promise.cause();
                }

            }
        });
        return (T) service;
    }


    @GetMapping("getUser")
    public String getUser(@RequestParam(name = "id") String userId)  {


        Class<UserService> serviceClass = UserService.class;
        UserService userService = getProxyService(serviceClass);
        String user = userService.getUser(userId);
        return user;
    }
    @GetMapping("getUsers")
    public Map<String, User> getUser()  {


        Class<UserService> serviceClass = UserService.class;
        UserService userService = getProxyService(serviceClass);
        Map<String, User> user = userService.getUser();
        return user;
    }

}
