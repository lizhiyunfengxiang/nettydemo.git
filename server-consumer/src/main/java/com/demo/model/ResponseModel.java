package com.demo.model;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResponseModel {

    private String responseId;
    private String serviceName;
    private String methodName;
    private String code;
    private String data;
}
