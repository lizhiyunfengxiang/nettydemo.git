package com.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RequestModel {

    private String requestId;
    private String serviceName;
    private String methodName;
    private Class[] paramTypes;
    private Object[] paramValues;

}
